# Tails Server

This is only a preview. There is no actual functionality implemented yet.

## Setup

    sudo ./setup.py install

## Start the GUI

    sudo tails-server
    
## Try the CLI

    sudo tails-service    
    
## Uninstall

    sudo ./uninstall.py
