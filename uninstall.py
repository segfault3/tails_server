#!/usr/bin/env python3

import os
import shutil
import site

from tails_server import config
DATA_DIR = config.DATA_DIR

packages_dir = site.getsitepackages()[0]

package_path = os.path.join(packages_dir, "tails_server")
print("Removing %s" % package_path)
shutil.rmtree(package_path)

egg_info_file = os.path.join(packages_dir, "Tails_Server-0.1.egg-info")
print("Removing %s" % egg_info_file)
os.remove(egg_info_file)

scripts = ["tails-service", "tails-server"]
for script in scripts:
    path = os.path.join("/usr/local/bin", script)
    print("Removing %s" % path)
    os.remove(path)

print("Removing %s" % DATA_DIR)
shutil.rmtree(DATA_DIR)