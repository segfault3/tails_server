#!/usr/bin/env python3

import os
from gi.repository import Gtk

PACKAGE_PATH = os.path.dirname(os.path.realpath(__file__))


class TailsServerGUI(object):
    def on_window1_destroy(self, object, data=None):
        Gtk.main_quit()

    def on_onion_address_icon_clicked(self, entry, icon, event):
        entry.select_region(0, -1)
        entry.copy_clipboard()

    def on_close_clicked(self, button):
        Gtk.main_quit()

    def __init__(self):
        self.gladefile = os.path.join(PACKAGE_PATH, "tails_server.glade")
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)
        self.builder.connect_signals(self)
        self.window = self.builder.get_object("window1")
        self.window.connect("delete-event", Gtk.main_quit)
        self.window.set_title("Tails Server")
        self.window.show_all()


if __name__ == "__main__":
    tails_server_gui = TailsServerGUI()
    Gtk.main()
