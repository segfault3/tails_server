#!/usr/bin/env python3

import argparse
import os
import shutil
import sys
import subprocess

TORRC = "/etc/tor/torrc"
TOR_DIR = "/var/lib/tor"
TOR_USER = "debian-tor"


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", required=True, help="Will be used for the hidden service directory name")
    parser.add_argument("--local-port", required=True)
    parser.add_argument("--remote-port")
    parser.add_argument("--authorize_client", action="store_true", help="Use HiddenServiceAuthorizeClient")
    parser.add_argument("--authorize_client_stealth", action="store_true",
                        help="Use HiddenServiceAuthorizeClient with stealth option")
    parser.add_argument("--users", nargs="*", default="user",
                        help="User names to use with HiddenServiceAuthorizeClient")
    args = parser.parse_args()
    if not args.remote_port:
        args.remote_port = args.local_port
    return args


def create_hs_dir(name):
    path = os.path.join(TOR_DIR, name)
    try:
        os.mkdir(path, mode=0o600)
    except FileExistsError:
        # The UID of debian-tor might change between Tails releases (it did before)
        # This would cause existing persistent directories to have wrong UIDs, so we reset them here
        for dirpath, _, filenames in os.walk(path):
            shutil.chown(os.path.join(dirpath, filenames), TOR_USER, TOR_USER)
    shutil.chown(path, TOR_USER, TOR_USER)
    return path


def add_service_to_torrc(hs_dir, local_port, remote_port, authorize_client, authorize_client_stealth, users):
    lines = [
        "HiddenServiceDir %s" % hs_dir,
        "HiddenServicePort %s 127.0.0.1:%s" % (remote_port, local_port)
    ]
    if authorize_client_stealth:
        lines.append("HiddenServiceAuthorizeClient stealth %s" % ",".join(users))
    elif authorize_client:
        lines.append("HiddenServiceAuthorizeClient basic %s" % ",".join(users))

    for line in lines:
        write_line_if_not_present(TORRC, line)


def write_line_if_not_present(file_path, line):
    with open(TORRC, 'r+') as f:
        if line in f.read():
            return
        f.write(line)


def reload_tor():
    # TODO: Is there a way to restart services with systemd without the shell?
    tor_running = subprocess.check_call("systemctl is-active tor@default.service")
    if not tor_running:
        sys.exit("Tor is not running")
    subprocess.check_call("systemctl reload tor@default.service")


def main():
    args = parse_args()
    hs_dir = create_hs_dir(args.name)
    add_service_to_torrc(hs_dir, args.local_port, args.remote_port, args.authorize_client,
                         args.authorize_client_stealth, args.users)
    reload_tor()


if __name__ == "__main__":
    main()