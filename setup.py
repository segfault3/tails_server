#!/usr/bin/env python3

import os
import subprocess

REQUIRED_PACKAGES = ["python3-sh", "python3-yaml", "python3-gi", "python3-dbus", "python3-apt"]

subprocess.check_call(["apt-get", "install"] + REQUIRED_PACKAGES)

from distutils.core import setup

from tails_server import config

DATA_DIR = config.DATA_DIR
OPTIONS_DIR = config.OPTIONS_DIR

old_umask = os.umask(0o022)

setup(
    name='Tails Server',
    version='0.1',
    # description='Tails Server',
    # author='Tails Contributors',
    # author_email='tails@boum.org',
    url='tails.boum.org',
    packages=['tails_server', 'tails_server.gui', 'tails_server.services'],
    # package_data={'tails_server.gui': ['tails_server.gui/*.ui']},
    scripts=['scripts/tails-server', 'scripts/tails-service'],
    requires=['sh', 'yaml', 'gi', 'dbus', 'apt', 'stem']
)

import sh

def copy_dir_to_data_dir(dir_name):
    if not os.path.isdir(DATA_DIR):
        print("Creating directory %s" % DATA_DIR)
        os.mkdir(DATA_DIR)
    src = os.path.join("tails_server", dir_name)
    print("Copying %s -> %s" % (src, DATA_DIR))
    sh.cp("-r", src, DATA_DIR)
    # for root, dirs, files in os.walk(os.path.join(DATA_DIR, dir_name)):
    #     for d in dirs:
    #         os.chmod(os.path.join(root, d), 0o755)
    #     for f in files:
    #         os.chmod(os.path.join(root, f), 0o644)


copy_dir_to_data_dir("services")
copy_dir_to_data_dir("gui")
copy_dir_to_data_dir("icons")

sh.mkdir("-p",OPTIONS_DIR)

for service in os.listdir(os.path.join(DATA_DIR, 'services')):
    os.chmod(os.path.join(DATA_DIR, 'services', service), 0o755)

os.umask(old_umask)
